#!/usr/bin/env bash

days=' '$(date -d monday +%a)' '$(date -d tuesday +%a)' '$(date -d wednesday +%a)' '$(date -d thursday +%a)' '$(date -d friday +%a)' '$(date -d saturday +%a)' '$(date -d sunday +%a)' ' # this should work for international system locales

hours='01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 00' 

echo " "
echo "                          ****** Punch card for $(basename "$PWD") ******"
echo " "
echo "      ------------------------------------time of day (hours)-----------------------------------------"
echo -n "      "

for hour in $hours; do
   echo -n "| $hour"
done
echo "| Total"

total_commits=0
total_daily_commits=0
count_hours=0

for day in $days; do
  echo " "
  echo -n " $day -"  
  for hour in $hours; do
    number_hourly_commits=$(git log --pretty=format:"%ad" --date=local --date=format:'%a %H' | grep "$day" | grep "$hour" | wc -l) # output only weekday and hour
    if [ $number_hourly_commits -eq 0 ]; then
      echo -n "  $number_hourly_commits " #  do not colorize 
    elif [ $number_hourly_commits -lt 10 ]; then
      echo -n `tput setab 2``tput setaf 0`"  $number_hourly_commits "`tput sgr0` # colorize green 
    elif [ $number_hourly_commits -lt 100 ]; then
      echo -n `tput setab 3``tput setaf 0`" $number_hourly_commits "`tput sgr0` # colorize yellow
    else
      echo -n `tput setab 1``tput setaf 0`" $number_hourly_commits"`tput sgr0` # colorize red
    fi

    let total_commits=total_commits+number_hourly_commits
    let total_daily_commits=total_daily_commits+number_hourly_commits
  done
  
  count_hours=0

  echo -n "|  $total_daily_commits " #  do not colorize 
  total_daily_commits=0 
done

echo " "
echo " "
echo "total_commits = $total_commits"
echo " "
echo " "

# git-logger is free software and is licensed under the GNU GPLv2+. See 'license' for copying conditions
# (C) 2018 Nuno Ferreira

